#!/bin/bash

config_file=/opt/aavs/config/eda2.yml
if [[ -n "$1" && "$1" != "-" ]]; then
   config_file="$1"
fi


# Script which can be put into CRONTAB 

# environment :
source ~/aavs-calibration/runtime_eda2.sh

dt=`date +%Y%m%d_%H%M`

cd /data/real_time_calibration
# In order to not use holography - remove option --holography below:
echo "~/aavs-calibration/calibration_loop_eda2.sh \"--beam_correct --holography\" 0 ${config_file} > ${dt}_calibration.log 2>&1"
~/aavs-calibration/calibration_loop_eda2.sh "--beam_correct --holography" 0 ${config_file} > ${dt}_calibration.log 2>&1

