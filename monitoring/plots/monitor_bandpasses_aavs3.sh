#!/bin/bash

# source /opt/aavs/python/bin/activate

echo "python /opt/aavs/bin/monitor_bandpasses.py --interface=eno8303 --config=/opt/aavs/config/aavs3.yml --plot_directory=/data/monitoring/aavs3/bandpass/"
python /opt/aavs/bin/monitor_bandpasses.py --interface=eno8303 --config=/opt/aavs/config/aavs3.yml --plot_directory=/data/monitoring/aavs3/bandpass/
