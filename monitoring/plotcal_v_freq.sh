#!/bin/bash

# caldir="/data/aavs2/real_time_calibration/"
station_name="eda2"
station_id=2
if [[ -n "$1" && "$1" != "-" ]]; then
   station_name="$1"
fi
station_name_lower=`echo $station_name | awk '{print tolower($1);}'`
if [[ $station_name_lower == "aavs2" ]]; then
   station_id=3
fi
if [[ $station_name_lower == "aavs3" ]]; then
   station_id=5 # make it harder not to hardcode ...
fi


data_dir="./"
if [[ -n "$2" && "$2" != "-" ]]; then
   data_dir=$2
   cd ${data_dir}
fi

# but on aavs1-server
# 2022-02-24 : changed back to /exports/calibration/ - but may require to be set as below for the antenna health monitor :
# www_dir="/exports/calibration/${station_name}/antenna_health/"
www_dir="/html/calibration/"
# remote_server="aavs@aavs1-server"
# server_prefix="${remote_server}:"
# 2024-05-23 : change for EDA2 where WWW server is running on another computer but drive is mounted via NFS -> no server prefix
remote_server=""
server_prefix=""
if [[ $station_name_lower == "aavs3" ]]; then
   www_dir="/data/www/calibration/"
   remote_server=""
   server_prefix=""
fi
if [[ -n "$3" && "$3" != "-" ]]; then
   www_dir=$3
fi

fittime=
if [[ -n "$4" && "$4" != "-" ]]; then
   fittime=$4
fi

if [[ -n "$5" && "$5" != "-" ]]; then
   remote_server="$5"
   server_prefix="${remote_server}:"
fi


echo "################################################"
echo "PARAMETERS:"
echo "################################################"
echo "station_name = $station_name ( station_id = $station_id )"
echo "fittime      = $fittime"
echo "remove_server = $remote_server"
echo "www_dir       = $www_dir"
echo "################################################"


curr_path=`pwd`
cal_dtm=`basename $curr_path`
echo "Real-time calibration path = $curr_path -> cal_dtm = $cal_dtm"

if [[ -n "$fittime" && "$fittime" != "-" ]]; then
   echo "python ~/aavs-calibration/monitoring/plotcal_v_freq.py --station_id=${station_id} --fittime=\"$fittime\""
   python ~/aavs-calibration/monitoring/plotcal_v_freq.py --station_id=${station_id} --fittime="$fittime"
else
   echo "python ~/aavs-calibration/monitoring/plotcal_v_freq.py --station_id=${station_id}"
   python ~/aavs-calibration/monitoring/plotcal_v_freq.py --station_id=${station_id}
fi

# prepare directorios and copy images to WWW server :
images_dir=${www_dir}/${station_name_lower}/${cal_dtm}

if [[ -n "$remote_server" ]]; then
   echo "ssh ${remote_server} \"mkdir -p ${images_dir} ${www_dir}/${station_name_lower}/current\""
   ssh ${remote_server} "mkdir -p ${images_dir} ${www_dir}/${station_name_lower}/current"
else
   echo "INFO : www service runs on the local server"
   echo "mkdir -p ${images_dir} ${www_dir}/${station_name_lower}/current"
   mkdir -p ${images_dir} ${www_dir}/${station_name_lower}/current
fi

# echo "ssh aavs@aavs1-server \"ln -sf ${images_dir} ${www_dir}/${station_name_lower}/current\""
# ssh aavs@aavs1-server "ln -sf ${images_dir} ${www_dir}/${station_name_lower}/current"

echo "scp ~/aavs-calibration/monitoring/html/plot_calsol_index_${station_name_lower}.html ${server_prefix}${images_dir}/index.html"
scp ~/aavs-calibration/monitoring/html/plot_calsol_index_${station_name_lower}.html ${server_prefix}${images_dir}/index.html

echo "scp ~/aavs-calibration/monitoring/html/plot_calsol_index_${station_name_lower}.html ${server_prefix}${www_dir}/${station_name_lower}/current/index.html"
scp ~/aavs-calibration/monitoring/html/plot_calsol_index_${station_name_lower}.html ${server_prefix}${www_dir}/${station_name_lower}/current/index.html


echo "rsync -avP *.png ${server_prefix}${images_dir}/"
rsync -avP *.png ${server_prefix}${images_dir}/

echo "rsync -avP *.png ${server_prefix}${www_dir}/${station_name_lower}/current/"
rsync -avP *.png ${server_prefix}${www_dir}/${station_name_lower}/current/

# Get delays from DB and format them for the station config file:
# PGUSER=aavs
pwd
echo "python ~/aavs-calibration/monitoring/delay_per_tpm.py --db=$PGUSER --station_id=${station_id}"
python ~/aavs-calibration/monitoring/delay_per_tpm.py --db=$PGUSER --station_id=${station_id}

echo "rsync -avP delay_vs_tpm_lastdb.conf ${server_prefix}${www_dir}/${station_name_lower}/current/"
rsync -avP delay_vs_tpm_lastdb.conf ${server_prefix}${www_dir}/${station_name_lower}/current/

echo "rsync -avP delay_vs_tpm_lastdb.conf ${server_prefix}${images_dir}/"
rsync -avP delay_vs_tpm_lastdb.conf ${server_prefix}${images_dir}/
