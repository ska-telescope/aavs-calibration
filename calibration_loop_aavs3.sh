#!/bin/bash

options="" # was --skip-db
if [[ -n "$1" && "$1" != "-" ]]; then
   options="$1"
fi

do_init=0
if [[ -n "$2" && "$2" != "-" ]]; then
   do_init=$2
fi

init_options=""
if [[ -n "$3" && "$3" != "-" ]]; then
   init_options="$3"
fi


if [[ $do_init -gt 0 ]]; then
   echo "python  /opt/aavs/bin/station.py --config=/opt/aavs/config/aavs3.yml -IP ${init_options}"
   python  /opt/aavs/bin/station.py --config=/opt/aavs/config/aavs3.yml -IP ${init_options}

   # WARNING : second time is required due to some bug in /opt/aavs/bin/station.py related to log files 
   #           which causes issues with the 1st initalisation
   echo "python  /opt/aavs/bin/station.py --config=/opt/aavs/config/aavs3.yml -IP ${init_options}"
   python  /opt/aavs/bin/station.py --config=/opt/aavs/config/aavs3.yml -IP ${init_options}
else
   echo "WARNING : station initialisation is not required"
fi


# typical paremeters to execute a single calibration loop on the AAVS2 data (at the moment in debug mode : 
echo "python ~/aavs-calibration/calibration_loop.py --config=/opt/aavs/config/aavs3.yml -i enp65s0f0np0 -d /data/real_time_calibration/ --daq_library=/home/ubuntu/Software/aavs-system/src/build_2seconds/libaavsdaq.so ${options}"
python ~/aavs-calibration/calibration_loop.py --config=/opt/aavs/config/aavs3.yml -i enp65s0f0np0 -d /data/real_time_calibration/ --daq_library=/home/ubuntu/Software/aavs-system/src/build_2seconds/libaavsdaq.so ${options}

echo "post_calibration_actions.sh AAVS3"
post_calibration_actions.sh AAVS3 
