#!/bin/bash

config_file=/opt/aavs/config/eda2.yml
if [[ -n "$1" && "$1" != "-" ]]; then
   config_file="$1"
fi

semaphore_file=/data/real_time_calibration/calrunning.txt
if [[ -s ${semaphore_file} ]]; then
   echo "WARNING : calibration already started at unixtime :"
   cat ${semaphore_file}
   
   echo "WARNING : if you think it's wrong remove the semaphore file ${semaphore_file}, exiting now"
   exit
fi

uxstart=`date %s`
echo "date $uxstart > ${semaphore_file}"
date $uxstart > ${semaphore_file}

# Script which can be put into CRONTAB 

# environment :
source ~/aavs-calibration/runtime_eda2.sh

dt=`date +%Y%m%d_%H%M`

cd /data/real_time_calibration
# In order to not use holography - remove option --holography below:
echo "~/aavs-calibration/calibration_loop_eda2.sh \"--beam_correct\" 2 ${config_file} > ${dt}_calibration.log 2>&1"
~/aavs-calibration/calibration_loop_eda2.sh "--beam_correct" 2 ${config_file} > ${dt}_calibration.log 2>&1

echo "rm -f ${semaphore_file}"
rm -f ${semaphore_file}
