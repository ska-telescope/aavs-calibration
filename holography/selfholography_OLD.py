#!/usr/bin/env python3

"""
Perform Beam-Correlation self-holography with visibilities, based on Thekkeppattu et.al.(2024). 
Accepts AAVS2 or EDA2 single channel UVFITS files. Requires the antenna locations text files for AAVS2 and EDA2.
Uses Sun as the calibrator, so the visibility data should have Sun within the FoV, preferably close to the zenith.
Reference :  https://doi.org/10.1029/2023RS007847
"""

__author__ = "Jishnu N. Thekkeppattu"         

import argparse
import sys
parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('-f', "--filename", help='UVFITS filename with PATH, no defaults')
parser.add_argument('-e', "--elevation", help='Set the elevation threshold in degrees, default is 45 degrees', nargs='?', default=45, type=float)
parser.add_argument('-d', "--dynarange", help='Set the dynamic range of aperture images in dB, default is 16dB', nargs='?', default=16, type=float)
parser.add_argument("--font", nargs='?', default=14, type=int, help='Set a font size for the text used in plots (integer), default is 14')
parser.add_argument("--anname", action='store_true', help='Flag to overlay antenna names (such as Ant061) instead of antenna numbers (1-256)')
parser.add_argument("--antenna_file", dest="antenna_file", default="antenna_locations.txt",help='Name of file with antenna positions')
parser.add_argument("--show", action='store_true', help='Flag to show all the plots')
parser.add_argument("--conjugate", action='store_true', help='Flag to conjugate the visibilities. Experimental, can open a can of worms !')
parser.add_argument("--colourbar", action='store_true', help='Flag to enable colourbars in figures')
args = parser.parse_args(args=None if sys.argv[1:] else ['--help'])

import numpy as np
import astropy.io.fits as fits
from astropy.coordinates import EarthLocation, AltAz, get_sun
from astropy import units as u
from astropy.time import Time
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.ticker as ticker
plt.rcParams.update({'font.size': int(args.font)})
dosave = True # Setting to True always

def split_baseline(bline_number):
    if bline_number >= 65536:
        ant1 = int((bline_number - 65536) // 2048)
        ant2 = int((bline_number - 65536) % 2048)
    else:
        ant1 = int(bline_number // 256)
        ant2 = int(bline_number % 256)
    return (ant1,ant2)

if (args.anname):
    an_overlay = True
else:
    an_overlay = False

if (args.colourbar):
    cbar_enable = True
else:
    cbar_enable = False

filename = args.filename
if not filename:
    print ("Please provide a filename, quitting ...")
    sys.exit(0)
try:
    d = fits.open(filename)
except:
    print ("Could not open the file, quitting ...")
    sys.exit(0)

dyna_range_dB = float(args.dynarange)

fhead = filename.split(".")[0]
filename_nopath = filename.split("/")[-1]

elevation_limit = float(args.elevation)
telescope = d[0].header['TELESCOP']
freq = float(d[1].header['FREQ'])/1e6


# if telescope == "AAVS2":
#    antenna_file = "antenna_locations_aavs3.txt"
#elif telescope == "EDA2":
#    antenna_file = "antenna_locations_eda2.txt"
# elif telescope == "AAVS3":
#     antenna_file = "antenna_locations_aavs3.txt"
#else:
#    print ("Unrecognised telescope, quitting ...")
#    sys.exit(0)

print ("Telescope : "+str(telescope))
print ("Frequency : "+ str(freq)+" MHz")

an_loc = np.loadtxt(args.antenna_file, usecols=range(1,3))
f = open(args.antenna_file, "r")
i = 0
antenna_locations = {}
antenna_name = {}    
for line in f:
    line = line.strip()
    line = line.split()
    if line[0]!='#':
        antenna_locations[line[0]] = an_loc[i]
        antenna_name[str(int(i+1))] = line[0]
        i = i+1
f.close()
Nants = an_loc.shape[0]

uvtable = d[0].data
vis = uvtable['DATA']
Nvis = vis.shape[0]

x_pol_covmatrix = np.zeros([Nants, Nants], dtype=np.complex64)
y_pol_covmatrix = np.zeros([Nants, Nants], dtype=np.complex64)

if(args.conjugate):
    print ("Will conjugate the visibilies")
    bf_sign = -1
else:
    bf_sign = 1

for i in range(Nvis):
    baseline = int(uvtable['BASELINE'][i])
    antenna1, antenna2 = split_baseline(baseline)
    x_pol_covmatrix[antenna1-1][antenna2-1] = vis[i,0,0,0,0,0] + (bf_sign*1j*vis[i,0,0,0,0,1])
    x_pol_covmatrix[antenna2-1][antenna1-1] = vis[i,0,0,0,0,0] - (bf_sign*1j*vis[i,0,0,0,0,1])

    y_pol_covmatrix[antenna1-1][antenna2-1] = vis[i,0,0,0,1,0] + (bf_sign*1j*vis[i,0,0,0,1,1])
    y_pol_covmatrix[antenna2-1][antenna1-1] = vis[i,0,0,0,1,0] - (bf_sign*1j*vis[i,0,0,0,1,1])

#Here we calculate the weights to point the station beam towards the Sun
time_here = Time(uvtable['DATE'][0], format='jd')
MRO_WA = EarthLocation(lat=-26.70312*u.deg, lon=116.670575*u.deg, height=0*u.m)
altazframe = AltAz(obstime=time_here, location=MRO_WA)                                               
pos_Sun = get_sun(time_here)
print ("Sun position at MRO")
print ("Sun RA  :",pos_Sun.ra.hms.h,"h",pos_Sun.ra.hms.m,"m")
print ("Sun Dec : {:.2f} degrees".format(pos_Sun.dec.deg))
sunaltaz = pos_Sun.transform_to(altazframe) 
sun_alt = sunaltaz.alt.value*np.pi/180
sun_az = sunaltaz.az.value*np.pi/180
print ("Sun Elevation : {:.2f} degrees".format(sunaltaz.alt.value))

if (sunaltaz.alt.value < elevation_limit):
    print ("Sun elevation too low, quitting ...")
    sys.exit(0)

lm_sun = np.array([np.cos(sun_alt)*np.sin(sun_az), np.cos(sun_alt)*np.cos(sun_az)])
weight_vector_Sun = np.exp(-1j*(bf_sign)*2*np.pi*np.einsum('ij,j->i', an_loc, lm_sun)/(300/(freq)))

res_ele = (300/freq)/(40) #AAVS2 has a diameter of 35m, therefore we choose the denominator to be 40m.
Ndim = int(4/res_ele)
Ndim = 2*int(Ndim/2) + 1 # To make it odd
lm_matrix = np.zeros([2,Ndim,Ndim], dtype=np.float64)
i = 0
for d_l in np.linspace(-res_ele*Ndim/2, res_ele*Ndim/2, Ndim):
    j = 0
    for d_m in np.linspace(-res_ele*Ndim/2, res_ele*Ndim/2, Ndim): 
        lm_matrix[:,i,j] = [d_l, d_m] 
        j = j+1
    i = i+1

aperture_dim = (300/freq)/float(lm_matrix[0,1,0] - lm_matrix[0,0,0])

weight_tensor = np.exp(-1j*(bf_sign)*2*np.pi*np.einsum('ij,jkl->ikl', an_loc, lm_matrix)/(300/freq))
print ("Weight tensor dimensions : ", weight_tensor.shape)

x_pol_sh_meas_corr_cov = np.einsum('i,il,l->i', weight_vector_Sun, x_pol_covmatrix, np.conj(weight_vector_Sun), optimize='optimal')
y_pol_sh_meas_corr_cov = np.einsum('i,il,l->i', weight_vector_Sun, y_pol_covmatrix, np.conj(weight_vector_Sun), optimize='optimal')

x_pol_beamcorr_sh_bc_cov = np.einsum('ijk,i->jk', weight_tensor, x_pol_sh_meas_corr_cov, optimize='optimal')
y_pol_beamcorr_sh_bc_cov = np.einsum('ijk,i->jk', weight_tensor, y_pol_sh_meas_corr_cov, optimize='optimal')

# x_pol_beamcorr_sh_bc_cov = np.einsum('ijk,i,il,l->jk', weight_tensor, weight_vector_Sun, x_pol_covmatrix, np.conj(weight_vector_Sun), optimize='optimal')
# y_pol_beamcorr_sh_bc_cov = np.einsum('ijk,i,il,l->jk', weight_tensor, weight_vector_Sun, y_pol_covmatrix, np.conj(weight_vector_Sun), optimize='optimal')
print ("Beam-Correlation dimensions : ", x_pol_beamcorr_sh_bc_cov.shape)

del(weight_tensor)

fig, ax_bc_xm = plt.subplots(1,1, figsize=(10,10))
ax_bc_xm.set_title("Non-padded Beam-Corr, X-pol (mag), " + str(filename_nopath))
ax_bc_xm.imshow(np.abs(x_pol_beamcorr_sh_bc_cov), cmap='inferno')
if dosave:
    fig.savefig(fhead+"_bc_xm.png", format='png', dpi=300, bbox_inches='tight')

fig, ax_bc_ym = plt.subplots(1,1, figsize=(10,10))
ax_bc_ym.set_title("Non-padded Beam-Corr, Y-pol (mag), " + str(filename_nopath))
ax_bc_ym.imshow(np.abs(y_pol_beamcorr_sh_bc_cov), cmap='inferno')
if dosave:
    fig.savefig(fhead+"_bc_ym.png", format='png', dpi=300, bbox_inches='tight')

print("Beam-Correlations saved, now plotting aperture images ..")

sigma = 1.0
window2d = (1/np.sqrt(2*np.pi*sigma**2))*np.exp(-(1/(2*sigma**2))*(lm_matrix[0,:,:]**2 + lm_matrix[1,:,:]**2))

NFFT = 513
pad_len = int((NFFT-Ndim)/2)

x_pol_padded_image_sh_bc_cov = np.pad(x_pol_beamcorr_sh_bc_cov*window2d, ((pad_len,pad_len),(pad_len,pad_len)), 'constant') 
y_pol_padded_image_sh_bc_cov = np.pad(y_pol_beamcorr_sh_bc_cov*window2d, ((pad_len,pad_len),(pad_len,pad_len)), 'constant') 

x_pol_aperture_image_sh_bc_cov = np.fft.ifftshift((np.fft.ifft2(np.fft.ifftshift(x_pol_padded_image_sh_bc_cov), [NFFT,NFFT])))
y_pol_aperture_image_sh_bc_cov = np.fft.ifftshift((np.fft.ifft2(np.fft.ifftshift(y_pol_padded_image_sh_bc_cov), [NFFT,NFFT])))

tick_labels = np.linspace(-18, 18, 5).astype(np.int16)
tick_locs = (tick_labels*NFFT/aperture_dim).astype(np.int16) + (NFFT/2)

# We create mask files for phase images to obtain more useful figures
mask_indices_xpol = np.where(np.abs(x_pol_aperture_image_sh_bc_cov)>np.mean(np.abs(x_pol_aperture_image_sh_bc_cov))*3)
mask_xpol = np.ones(x_pol_aperture_image_sh_bc_cov.shape)
mask_xpol[mask_indices_xpol] = 0

mask_indices_ypol = np.where(np.abs(y_pol_aperture_image_sh_bc_cov)>np.mean(np.abs(y_pol_aperture_image_sh_bc_cov))*3)
mask_ypol = np.ones(y_pol_aperture_image_sh_bc_cov.shape)
mask_ypol[mask_indices_ypol] = 0

# centrezerocmap = mpl.colormaps.get_cmap("viridis").copy()
# centrezerocmap.set_bad(color='black')

##########################################################################

fig, ax_xm = plt.subplots(1,1, figsize=(20,20))

aperture_mag_plot = 10*np.log10(np.abs(x_pol_aperture_image_sh_bc_cov)/np.max(np.abs(x_pol_aperture_image_sh_bc_cov)))
mesh = ax_xm.imshow(np.flipud(aperture_mag_plot.T), cmap='inferno', vmin=np.max(aperture_mag_plot)-dyna_range_dB)
if cbar_enable:
    fig.colorbar(mesh, ax=ax_xm, orientation='horizontal', fraction=0.046, pad=0.08) #aspect=40,
for i in range(1,257):
    anname = antenna_name[str(i)]
    an_x = (NFFT/2) + (antenna_locations[anname][0])*NFFT/aperture_dim + 1
    an_y = (NFFT/2) - (antenna_locations[anname][1])*NFFT/aperture_dim - 1
    ax_xm.scatter(an_x, an_y, s=300, facecolors='none', edgecolors='white')
    if (an_overlay):
        ax_xm.annotate(anname, (an_x+2, an_y-2), color='white')
    else:
        ax_xm.annotate(str(i), (an_x+4, an_y-4), color='white')
ax_xm.xaxis.set_ticks(tick_locs)
ax_xm.xaxis.set_ticklabels(tick_labels)
ax_xm.yaxis.set_ticks(tick_locs)
ax_xm.yaxis.set_ticklabels(np.flip(tick_labels))
ax_xm.set_xlabel("South")
ax_xm.set_ylabel("West")
ax_xm.set_title(telescope+" Self-Holo, "+str(filename_nopath)+", X-pol Magnitude (dB)")

if dosave:
    fig.savefig(fhead+"_sh_xm.png", format='png', dpi=300, bbox_inches='tight')

##########################################################################

fig, ax_ym = plt.subplots(1,1, figsize=(20,20))

aperture_mag_plot = 10*np.log10(np.abs(y_pol_aperture_image_sh_bc_cov)/np.max(np.abs(y_pol_aperture_image_sh_bc_cov)))
mesh = ax_ym.imshow(np.flipud(aperture_mag_plot.T), cmap='inferno', vmin=np.max(aperture_mag_plot)-dyna_range_dB)
if cbar_enable:
    fig.colorbar(mesh, ax=ax_ym, orientation='horizontal', fraction=0.046, pad=0.08) #aspect=40,
for i in range(1,257):
    anname = antenna_name[str(i)]
    an_x = (NFFT/2) + (antenna_locations[anname][0])*NFFT/aperture_dim + 1
    an_y = (NFFT/2) - (antenna_locations[anname][1])*NFFT/aperture_dim - 1
    ax_ym.scatter(an_x, an_y, s=300, facecolors='none', edgecolors='white')
    if (an_overlay):
        ax_ym.annotate(anname, (an_x+2, an_y-2), color='white')
    else:
        ax_ym.annotate(str(i), (an_x+4, an_y-4), color='white')
ax_ym.xaxis.set_ticks(tick_locs)
ax_ym.xaxis.set_ticklabels(tick_labels)
ax_ym.yaxis.set_ticks(tick_locs)
ax_ym.yaxis.set_ticklabels(np.flip(tick_labels))
ax_ym.set_xlabel("South")
ax_ym.set_ylabel("West")
ax_ym.set_title(telescope+" Self-Holo, "+str(filename_nopath)+", Y-pol Magnitude (dB)")

if dosave:
    fig.savefig(fhead+"_sh_ym.png", format='png', dpi=300, bbox_inches='tight')

##########################################################################

fig, ax_xp = plt.subplots(1,1, figsize=(20,20))
aperture_phase_plot = np.ma.masked_array(np.angle(x_pol_aperture_image_sh_bc_cov)*180/np.pi, mask=mask_xpol)
mesh = ax_xp.imshow(np.flipud(aperture_phase_plot.T) ) # cmap='nipy_spectral'
if cbar_enable:
    fig.colorbar(mesh, ax=ax_xp, orientation='horizontal', fraction=0.046, pad=0.08) #aspect=40,
for i in range(1,257):
    anname = antenna_name[str(i)]
    an_x = (NFFT/2) + (antenna_locations[anname][0])*NFFT/aperture_dim + 1
    an_y = (NFFT/2) - (antenna_locations[anname][1])*NFFT/aperture_dim - 1
    ax_xp.scatter(an_x, an_y, s=300, facecolors='none', edgecolors='white')
    if (an_overlay):
        ax_xp.annotate(anname, (an_x+2, an_y-2), color='white')
    else:
        ax_xp.annotate(str(i), (an_x+4, an_y-4), color='white')
ax_xp.xaxis.set_ticks(tick_locs)
ax_xp.xaxis.set_ticklabels(tick_labels)
ax_xp.yaxis.set_ticks(tick_locs)
ax_xp.yaxis.set_ticklabels(np.flip(tick_labels))
ax_xp.set_xlabel("South")
ax_xp.set_ylabel("West")
ax_xp.set_title(telescope+" Self-Holo, "+str(filename_nopath)+", X-pol Phase (degrees)")

if dosave:
    fig.savefig(fhead+"_sh_xp.png", format='png', dpi=300, bbox_inches='tight')

##########################################################################

fig, ax_yp = plt.subplots(1,1, figsize=(20,20))

aperture_phase_plot = np.ma.masked_array(np.angle(y_pol_aperture_image_sh_bc_cov)*180/np.pi, mask=mask_ypol)
mesh = ax_yp.imshow(np.flipud(aperture_phase_plot.T) ) # cmap='nipy_spectral'
if cbar_enable:
    fig.colorbar(mesh, ax=ax_yp, orientation='horizontal', fraction=0.046, pad=0.08) #aspect=40,
for i in range(1,257):
    anname = antenna_name[str(i)]
    an_x = (NFFT/2) + (antenna_locations[anname][0])*NFFT/aperture_dim + 1
    an_y = (NFFT/2) - (antenna_locations[anname][1])*NFFT/aperture_dim - 1
    ax_yp.scatter(an_x, an_y, s=300, facecolors='none', edgecolors='white')
    if (an_overlay):
        ax_yp.annotate(anname, (an_x+2, an_y-2), color='white')
    else:
        ax_yp.annotate(str(i), (an_x+4, an_y-4), color='white')
ax_yp.xaxis.set_ticks(tick_locs)
ax_yp.xaxis.set_ticklabels(tick_labels)
ax_yp.yaxis.set_ticks(tick_locs)
ax_yp.yaxis.set_ticklabels(np.flip(tick_labels))
ax_yp.set_xlabel("South")
ax_yp.set_ylabel("West")
ax_yp.set_title(telescope+" Self-Holo, "+str(filename_nopath)+", Y-pol Phase (degrees)")

if dosave:
    fig.savefig(fhead+"_sh_yp.png", format='png', dpi=300, bbox_inches='tight')

print("Aperture images saved")

##########################################################################
CB_color_cycle = ['#377eb8', '#ff7f00', '#4daf4a',
                  '#f781bf', '#a65628', '#984ea3',
                  '#999999', '#e41a1c', '#dede00']

fig, ax = plt.subplots(2,2, figsize=(20,12))
fig.suptitle(telescope+" Self-Holo, "+str(filename_nopath))

ax[0][0].set_title("X pol gain magnitude")
ax[0][0].plot(np.abs(x_pol_sh_meas_corr_cov), label=r"$|h|+0.4$,Antenna-Correlation using visibilities", color=CB_color_cycle[2])
ax[0][0].legend(loc='upper left')
ax[0][0].set_xlabel(r"Antenna number $\eta$")
ax[0][0].set_ylabel(r"Normalised magnitude")
ax[0][0].set_xlim(0,255)
# ax[0][0].set_ylim(-0.1,4)

ax[0][1].set_title("Y pol gain magnitude")
ax[0][1].plot(np.abs(y_pol_sh_meas_corr_cov), label=r"$|h|+0.4$,Antenna-Correlation using visibilities", color=CB_color_cycle[6])
ax[0][1].legend(loc='upper left')
ax[0][1].set_xlabel(r"Antenna number $\eta$")
ax[0][1].set_ylabel(r"Normalised magnitude")
ax[0][1].set_xlim(0,255)
# ax[0][1].set_ylim(-0.1,4)

ax[1][0].set_title("X pol gain phase")
ax[1][0].plot(np.angle(x_pol_sh_meas_corr_cov), label=r"$\measuredangle h+1.5$,Antenna-Correlation using visibilities", color=CB_color_cycle[2])
ax[1][0].legend(loc='upper left')
ax[1][0].set_xlabel(r"Antenna number $\eta$")
ax[1][0].set_ylabel(r"Phase (radians)")
ax[1][0].set_xlim(0,255)
ax[1][0].set_ylim(-3.14,3.14)

ax[1][1].set_title("Y pol gain phase")
ax[1][1].plot(np.angle(y_pol_sh_meas_corr_cov), label=r"$\measuredangle h+1.5$,Antenna-Correlation using visibilities", color=CB_color_cycle[6])
ax[1][1].legend(loc='upper left')
ax[1][1].set_xlabel(r"Antenna number $\eta$")
ax[1][1].set_ylabel(r"Phase (radians)")
ax[1][1].set_xlim(0,255)
ax[1][1].set_ylim(-3.14,3.14)
fig.tight_layout()
plt.subplots_adjust(wspace=0.2, hspace=0.3)

if dosave:
    fig.savefig(fhead+"_sh_meas_corr.png", format='png', dpi=300, bbox_inches='tight')

print("Measured correlations plot saved")

##########################################################################

antenna_name_list = np.array([])
for i in range(Nants):
    antenna_name_list = np.append(antenna_name_list, antenna_name[str(i+1)] )

pi2deg = 180/np.pi
# np.savetxt(fhead+"_meascorr.txt", np.column_stack([antenna_name_list, np.abs(x_pol_sh_meas_corr_cov), np.angle(x_pol_sh_meas_corr_cov)*pi2deg, 
#                                                    np.abs(y_pol_sh_meas_corr_cov), np.angle(y_pol_sh_meas_corr_cov)*pi2deg]),
#                                                    fmt='%s', header='antenna, mag(X), ang(X), mag(Y), ang(Y)', delimiter=",")

np.savetxt(fhead+"_meascorr_Xpol.txt", np.column_stack([np.abs(x_pol_sh_meas_corr_cov), np.angle(x_pol_sh_meas_corr_cov)*pi2deg]),
                                                   fmt='%.4f', delimiter=",")

np.savetxt(fhead+"_meascorr_Ypol.txt", np.column_stack([np.abs(y_pol_sh_meas_corr_cov), np.angle(y_pol_sh_meas_corr_cov)*pi2deg]),
                                                   fmt='%.4f', delimiter=",")
print("Saved pydbcal compatible gains files for X and Y pols")

##########################################################################

if (args.show):
    plt.show()