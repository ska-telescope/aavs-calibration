#!/bin/bash

do_conversion=1
if [[ -n "$1" && "$1" != "-" ]]; then
   do_conversion=$1
fi

conversion_options=""
if [[ -n "$2" && "$2" != "-" ]]; then
   conversion_options="$2"
fi

channel=204
station=aavs3
do_image=1
apply_cal=1
corrinttime=1.98181
corr_ch=1

# calibration .uv file:
# created with miriad_calibrate_uvfits.sh and do_xx_yy=1 :
# caluv=midday/chan_204_20231109T033845

# try to extract coarse channel from directory name
cc_dir=`pwd | tr -s '_/' '\n' | grep ch[0-9][0-9] | tr -d 'ch'`
# calculate the approximate system temp and system gain. Not strictly necessary, but may
# help with propoer noise-weighted calibration
# calculate approximate systemp as 180*(180/f)^2.55 + 50
systemp=`echo $cc_dir | awk '{ printf "%.1f\n", 180.0*(180/($1*0.781250))^2.55 + 50 }'`
# rough estimate of Ae for dipole/SKALA in sparse regime. Goes down at lambda^2 just due to collecting area
# of a dipole. Use the Ae at 140 MHz as reference, which is about 2.3 m^2 for both
Ae=2.3 # 140 MHz
#if [[ $cc_dir -gt 180 ]]; then
  # scale Ae down in sparse regime
#  Ae=`echo $cc_dir | awk '{ printf "%.1f\n", 2.3*($1*0.78125)^(-2) }'`
#fi
jyperk=`echo $Ae | awk '{ printf "%.1f\n", 2761./$1 }'`

echo "DEBUG : cc_dir = $cc_dir -> systemp = $systemp -> jyperk = $jyperk, Ae = $Ae"


if [[ $do_conversion -gt 0 ]]; then
  ls *.hdf5 > new_hdf5_list.txt
  echo "hdf5_to_uvfits_all.sh -c -l -i $corrinttime -n 1 -d "./" -N -a 1 -f ${channel} -L new_hdf5_list.txt -S 0 -s ${station} ${conversion_options} > conversion.out 2>&1"
  hdf5_to_uvfits_all.sh -c -l -i $corrinttime -n 1 -d "./" -N -a 1 -f ${channel} -L new_hdf5_list.txt -S 0 -s ${station} ${conversion_options} > conversion.out 2>&1
else
  echo "INFO : conversion is not required"
fi

########################################################## CALIBRATION ##########################################################
# sunhdf=`basename $middayfile _0.hdf5`
mkdir SunCal
cd SunCal

cp -s $AAVSCAL/config/${station}/antenna_locations.txt antenna_locations.txt
cp $AAVSCAL/config/${station}/instr_config.txt instr_config.txt
cp -s $AAVSCAL/config/${station}/header.txt header.txt

sunhdf="correlation_burst_204_20231115_08451_6"
ln -s ../${sunhdf}_ts_unix.txt
ln -s ../${sunhdf}.LCCSPC
cp ../${sunhdf}.LACSPC .

startunix=`cat ${sunhdf}_ts_unix.txt`

sunpos=`python $AAVSCAL/sunpos.py $startunix`
sunra=`echo $sunpos | cut -f 1 -d " "`
sundec=`echo $sunpos | cut -f 2 -d " "`

echo "Lfile2uvfits_eda.sh -i $corrinttime -n 2 -N 512 -C $corr_ch -f $cc_dir -R $sunra -D $sundec $sunhdf"
Lfile2uvfits_eda.sh -i $corrinttime -n 2 -N 512 -C $corr_ch -f $cc_dir -R $sunra -D $sundec $sunhdf

obsid=`ls chan_${cc_dir}_20*.uvfits | head -1`
src=sun${cc_dir}

# calculate the approximate system temp and system gain. Not strictly necessary, but may
# help with propoer noise-weighted calibration
# calculate approximate systemp as 180*(180/f)^2.55 + 50
systemp=`echo $cc_dir | awk '{ printf "%.1f\n", 180.0*(180/($1*0.781250))^2.55 + 50 }'`
# rough estimate of Ae for dipole/SKALA in sparse regime. Goes down at lambda^2 just due to collecting area
# of a dipole. Use the Ae at 140 MHz as reference, which is about 2.3 m^2 for both
Ae=2.3 # 140 MHz
if [ $cc_ind -gt 180 ] ; then
  # scale Ae down in sparse regime
  Ae=`echo $cc_ind | awk '{ printf "%.1f\n", 2.3*($1*0.78125)^(-2) }'`
fi
jyperk=`echo $Ae | awk '{ printf "%.1f\n", 2761./$1 }'`

rm -rf ${src}.uv
fits op=uvin in=${obsid} out=${src}.uv options=compress
puthd in=${src}.uv/jyperk value=$jyperk
puthd in=${src}.uv/systemp value=$systemp

# spectral indecis from Benz et al. :
# WARNING : not all ranges have been implemented below yet
#  50-100 MHz: 2.15 ->  24000,0.10,2.15
# 100-150 MHz: 1.86 ->  51000,0.15,1.6
# 150-200 MHz: 1.61 ->  51000,0.15,1.9
# 200-300 MHz: 1.50 ->  81000,0.20,1.5
# 300-400 MHz: 1.31 ->  149000,0.30,1.31
if [ $cc_dir -lt 192 ] ; then
  # reduce uv cut range for really low frequencies. Really only works when Sun is the only thing up at these freqs
  umin=0.004
  if [ $cc_dir -lt 100 ] ; then
    umin=0.003
  fi

  if [ $cc_dir -gt 128 ] ; then
     # 100 - 150 MHz :
     echo "Frequency range 100 - 150 MHz:"
     echo "mfcal vis=${src}.uv flux=51000,0.15,1.9 select='uvrange('$umin',1)' edge=2"
     mfcal vis=${src}.uv flux=51000,0.15,1.9 select='uvrange('$umin',1)' edge=2 # f < 150 MHz
  else
     # 50 - 100 MHz : 2.15 and 100 MHz :
     echo "Frequency range 50 - 100 MHz:"
     echo "mfcal vis=${src}.uv flux=24000,0.10,2.15 select='uvrange('$umin',1)' edge=2"
     mfcal vis=${src}.uv flux=24000,0.10,2.15 select='uvrange('$umin',1)' edge=2 # f 50 - 100 MHz
  fi
else
  echo "Frequency range >= 150 MHz:"
  echo "mfcal vis=${src}.uv flux=51000,0.15,1.6 select='uvrange(0.005,1)' edge=2"
  mfcal vis=${src}.uv flux=51000,0.15,1.6 select='uvrange(0.005,1)' edge=2 # f > 150 MHz
fi
puthd in=${src}.uv/interval value=2.5

#optionally image the sun
robust=0.5
stokes=xx
imsize=256
if [ $cc_ind -gt 300 ] ; then
  imsize=512
fi
rm -rf ${src}_${stokes}.map ${src}.beam
invert vis=${src}.uv map=${src}_${stokes}.map imsize=${imsize},${imsize} beam=${src}.beam robust=$robust options=double,mfs stokes=${stokes}
rm -rf ${src}_${stokes}.clean ${src}_${stokes}.restor ${src}_${stokes}.maxen
clean map=${src}_${stokes}.map beam=${src}.beam out=${src}_${stokes}.clean niters=5000 speed=-1 cutoff=100
restor model=${src}_${stokes}.clean beam=${src}.beam map=${src}_${stokes}.map out=${src}_${stokes}.restor
fits in=${src}_${stokes}.restor out=${src}_${stokes}.fits op=xyout

caluv=SunCal/${src}.uv
echo "Result, calibration in ${caluv}"
cd ../
########################################################## END OF CALIBRATION ##########################################################


# uvfits -> uv 
for uvfitsfile in `ls chan_*.uvfits` ; do
    src=`basename $uvfitsfile .uvfits`
    echo "Processing $uvfitsfile to ${src}.uv"
    rm -rf ${src}.uv ${src}_XX.uv ${src}_YY.uv
    
    echo "fits op=uvin in=\"$uvfitsfile\" out=\"${src}.uv\" options=compress"
    fits op=uvin in="$uvfitsfile" out="${src}.uv" options=compress
    
    echo "puthd in=${src}.uv/jyperk value=${jyperk}"
    puthd in=${src}.uv/jyperk value=${jyperk}
    
    echo "puthd in=${src}.uv/systemp value=${systemp}"
    puthd in=${src}.uv/systemp value=${systemp}
    
    # apply calibration :
    if [[ $apply_cal -gt 0 ]]; then
       echo "gpcopy vis=${caluv}.uv out=${src}.uv"
       gpcopy vis=${caluv}.uv out=${src}.uv
    else
       echo "WARNING : calibration could not be applied"
    fi

    if [[ $do_image -gt 0 ]]; then
       echo "uvcat vis=${src}.uv stokes=xx out=${src}_XX.uv"
       uvcat vis=${src}.uv stokes=xx out=${src}_XX.uv
       
       echo "uvcat vis=${src}.uv stokes=yy out=${src}_YY.uv"
       uvcat vis=${src}.uv stokes=yy out=${src}_YY.uv
       
       echo "invert vis=${src}_XX.uv map=${src}_XX.map imsize=180,180 beam=${src}_XX.beam robust=-0.5 options=double,mfs"
       invert vis=${src}_XX.uv map=${src}_XX.map imsize=180,180 beam=${src}_XX.beam robust=-0.5 options=double,mfs
       
       echo "invert vis=${src}_YY.uv map=${src}_YY.map imsize=180,180 beam=${src}_YY.beam robust=-0.5 options=double,mfs"
       invert vis=${src}_YY.uv map=${src}_YY.map imsize=180,180 beam=${src}_YY.beam robust=-0.5 options=double,mfs
       
       echo "fits op=xyout in=${src}_XX.map out=${src}_XX.fits"
       fits op=xyout in=${src}_XX.map out=${src}_XX.fits

       echo "fits op=xyout in=${src}_YY.map out=${src}_YY.fits"
       fits op=xyout in=${src}_YY.map out=${src}_YY.fits
    fi
done
