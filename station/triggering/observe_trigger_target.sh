#!/bin/bash

# environment variables :
# Eda2 server :
source /opt/aavs/python/bin/activate

export PATH=~/aavs-calibration/station/pointing:$PATH
export PATH=~/aavs-calibration/station/:~/aavs-calibration/:~/aavs-calibration/station/pointing:$PATH



freq_channel=256
if [[ -n "$1" && "$1" != "-" ]]; then
   freq_channel=$1
fi

object=J0437-4715
if [[ -n "$3" && "$3" != "-" ]]; then
   object=$3
fi

dir=`date +%Y%m%d%H%M%S`
data_dir=/data/triggering/${dir}/
if [[ -n "$2" && "$2" != "-" ]]; then
   data_dir=$2
fi
mkdir -p ${data_dir}

# checking if object is one of the pre-defined ones:
ra=69.3166
dec=-47.2525

if [[ -n "$4" && "$4" != "-" ]]; then
  ra=$4
fi

if [[ -n "$5" && "$5" != "-" ]]; then
  dec=$5
fi

interval=300
if [[ -n "$6" && "$6" != "-" ]]; then
   interval=$6
fi

start_uxtime=-1
if [[ -n "$7" && "$7" != "-" ]]; then
   start_uxtime=$7
fi

n_iter=1
if [[ -n "$8" && "$8" != "-" ]]; then
   n_iter=$8
fi

sleep_time=1800
if [[ -n "$9" && "$9" != "-" ]]; then
   sleep_time=$9
fi

pointing_interval=${interval}
if [[ -n "${10}" && "${10}" != "-" ]]; then
   pointing_interval=${10}
fi

repointing_resolution=30
if [[ -n "${11}" && "${11}" != "-" ]]; then
   repointing_resolution=${11}
fi

station=eda2
interface=enp216s0f0
if [[ -s /opt/aavs/config/station.yml ]]; then
   station=`awk -v station_section=0 '{if(index($1,":")>0 && NF==1){if(index($1,"station")>0 ){station_section=1;}else{station_section=0;}}if(station_section>0){if($1=="name:"){station_name=$2;gsub("\"","",station_name);gsub("\r","",station_name);print tolower(station_name);}}}' /opt/aavs/config/station.yml`
   echo "Station config file (or symbolik link) exists -> getting station_name = $station"
else
   echo "ERROR : /opt/aavs/config/station.yml file or symbolic link does not exist will use default station_name = $station or value passed in parameter -s"   
   exit;
fi
if [[ -n "${12}" && "${12}" != "-" ]]; then
   station=${12}
fi

ip=10.0.10.190
if [[ ${station} == "aavs2" ]]; then
   ip=10.0.10.210
fi
if [[ $station == "aavs3" ]]; then
   interface=enp65s0f0np0
   ip=10.137.0.103
fi



do_init_station=0
if [[ -n "${13}" && "${13}" != "-" ]]; then
   do_init_station=${13}
fi

full_time_resolution=1
if [[ -n "${14}" && "${14}" != "-" ]]; then
   full_time_resolution=${14}
fi

calibration_options="--flag_antennas=69,74,87,90,127,141,142,143,159,181,182,244,245"
if [[ -n "${15}" && "${15}" != "-" ]]; then
   calibration_options=${15}
fi

point_station=1
if [[ -n "${16}" && "${16}" != "-" ]]; then
   point_station=${16}
fi

calibrate_station=1
if [[ -n "${17}" && "${17}" != "-" ]]; then
   calibrate_station=${17}
fi

n_channels=40
daq_options="--start_channel 0 --nof_channels $n_channels --dada -I"
if [[ -n "${18}" && "${18}" != "-" ]]; then
   n_channels=${18}
fi
end_channel=-1
if [[ $n_channels -gt 0 && $full_time_resolution -gt 0 ]]; then
   end_channel=$(($ch+$n_channels))
#   daq_options="--start_channel 0 --nof_channels ${n_channels}"

   echo "INFO : adding extra DAQ options for acquire_station_beam program : $daq_options"
else
   echo "WARNING : no need for extra DAQ options (1 channel or not full time resolution ) for acquire_station_beam program : $daq_options"
fi

if [[ -n "${19}" && "${19}" != "-" ]]; then
   daq_options=${19}
fi

use_config_per_freq=1
if [[ -n "${20}" && "${20}" != "-" ]]; then
   use_config_per_freq=${20}
fi

config_file=/data/triggering/eda2_40channels_ch256.yml
if [[ -n "${21}" && "${21}" != "-" ]]; then
   config_file=${21}
fi

pointing_options="-"
if [[ -n "${22}" && "${22}" != "-" ]]; then
   pointing_options=${22}
fi

start_daq=1
if [[ -n "${23}" && "${23}" != "-" ]]; then
   start_daq=${23}   
fi

start_recording_ux=-1
if [[ -n "${24}" && "${24}" != "-" ]]; then
   start_recording_ux=${24}
fi

antfile=~/aavs-calibration/config/${station}/antenna_locations.txt
if [[ -n "${25}" && "${25}" != "-" ]]; then
   antfile="${25}"
fi


# 2023-06-07 (MS) : this code is to enable a different version of the acquisition program to be used:
#                   for example the new version with start time parameter (-C) compiled in : /home/amagro/station_beam_start_acq_time/aavs-system/src/build/
#                    !!! REMEMBER TO ALSO SET export LD_LIBRARY_PATH=/home/amagro/station_beam_start_acq_time/aavs-system/src/build/:$LD_LIBRARY_PATH
# ACQUIRE_STATION_BEAM_PATH=/opt/aavs/bin/acquire_station_beam
if [[ -n $ACQUIRE_STATION_BEAM_PATH ]]; then
   echo "INFO : Already defined : ACQUIRE_STATION_BEAM_PATH = $ACQUIRE_STATION_BEAM_PATH - using it"
else
   echo "WARNING : ACQUIRE_STATION_BEAM_PATH not defined -> setting now to:"
   export ACQUIRE_STATION_BEAM_PATH=/opt/aavs/bin/acquire_station_beam
   echo "ACQUIRE_STATION_BEAM_PATH = $ACQUIRE_STATION_BEAM_PATH"
fi


# RW/Chris Lee : I'm 99% sure the offset is 3 channels. (not 4). And that confirms the 3-channel offset also.
channel_from_start=4
ux=`date +%s`
start_uxtime_int=`echo $start_uxtime | awk '{printf("%d",$1);}'`
start_repointing=$(($start_uxtime_int+180))


echo "###################################################"
echo "PARAMETERS:"
echo "###################################################"
echo "station               = $station ( ip = $ip , interface = $interface )"
echo "interface            = $interface"
echo "Object                = $object"
echo "(ra,dec)              = ( $ra , $dec ) [deg]"
echo "freq_channel          = $freq_channel"
echo "data_dir              = $data_dir"
echo "interval              = $interval"
echo "pointing_interval     = $pointing_interval"
echo "start_uxtime          = $start_uxtime (start_repointing = $start_repointing)"
echo "n_iter                = $n_iter"
echo "sleep_time            = $sleep_time"
echo "repointing_resolution = $repointing_resolution"
echo "do_init_station       = $do_init_station"
echo "channel_from_start    = $channel_from_start"
echo "full_time_resolution  = $full_time_resolution"
echo "calibration_options   = $calibration_options"
echo "point_station         = $point_station"
echo "calibrate_station     = $calibrate_station"
echo "daq_options           = $daq_options"
echo "N channels = $n_channels -> end_channel = $end_channel"
echo "use_config_per_freq   = $use_config_per_freq"
echo "configuration file    = $config_file"
echo "ant positions file    = $antfile"
echo "current ux            = $ux"
echo "pointing_options      = $pointing_options"
echo "start_daq             = $start_daq"
echo "start_recording_ux    = $start_recording_ux (wait till this particular UNIX_TIME to start recording)" 
echo "ACQUIRE_STATION_BEAM_PATH = $ACQUIRE_STATION_BEAM_PATH"
echo "###################################################"

if [[ -s ${config_file} ]]; then
   echo "INFO : station configuration file $config_file OK"
else
   echo "ERROR : station configuration file $config_file does not exist -> cannot continue!"
   exit -1
fi

mkdir -p ${data_dir}
cd ${data_dir}

i=0
while [[ $i -lt 1 ]];
do
   echo
   echo "------------------------------------------------- i = $i -------------------------------------------------"
   date

   # kill old and start new pointing 
   echo "killall point_station_radec_loop.sh acquire_station_beam"
   killall point_station_radec_loop.sh acquire_station_beam 

   if [[ $point_station -gt 0 ]]; then
      # start pointing :      
      echo "INFO : starting station pointing scripts"
      pwd   
      echo "nohup ~/aavs-calibration/station/pointing/point_station_radec_loop.sh ${ra} ${dec} ${pointing_interval} ${repointing_resolution} ${station} ${object} ${pointing_options} -1 ${antfile} ${config_file} >> pointing.out 2>&1 &"
      nohup ~/aavs-calibration/station/pointing/point_station_radec_loop.sh ${ra} ${dec} ${pointing_interval} ${repointing_resolution} ${station} ${object} ${pointing_options} -1 ${antfile} ${config_file} >> pointing.out 2>&1 &
      pwd
      ps
   else
      echo "WARNING : pointing of station is not required (assuming it's done manually or not at all)"
   fi

   echo "echo ${freq_channel} > channel.txt"
   echo ${freq_channel} > channel.txt
   
   # waiting for pointing to start - not to collect data earlier !
   # 6 seconds seems to be a safe minimum to start pointing in the correct direction 
   echo "sleep 6"
   sleep 6

   # start acuisition :
   # WAS : /home/aavs/Software/aavs-system/src/build_new/acquire_station_beam

   if [[ $start_daq -gt 0 ]]; then   
      # set maximum file of 10 GB to avoid merging:
      echo "$ACQUIRE_STATION_BEAM_PATH -d ./ -t ${interval} -s 1048576 -c ${channel_from_start}  -i ${interface} -p ${ip} --max_file_size 10 --source ${object} ${daq_options} >> daq.out 2>&1"
      $ACQUIRE_STATION_BEAM_PATH -d ./ -t ${interval} -s 1048576 -c ${channel_from_start}  -i ${interface} -p ${ip} --max_file_size 10 --source ${object} ${daq_options} >> daq.out 2>&1         

      # leaving a file that it was done (with unixtime when finished)
      date +%s > done.txt
   else
      echo "WARNING : DAQ starting is not required -> please ensure DAQ is started manually !!!"
   fi
   
   # temporary due to the fact that that the program acquire_station_beam ends up with .dat files without group read permission:
   echo "chmod +r *.dat *.dada"
   chmod +r *.dat *.dada
   
   i=$(($i+1))
   echo "Iterations finished i = $i ( >= $n_iter ) at :"
done

