#!/bin/bash

export PATH=~/aavs-calibration/station/pointing:$PATH

freq_channel=256
if [[ -n "$1" && "$1" != "-" ]]; then
   freq_channel=$1
fi

dir=`date +%Y_%m_%d`
data_dir=`pwd`
if [[ -n "$2" && "$2" != "-" ]]; then
   data_dir=$2
fi
mkdir -p ${data_dir}

interval=1800
if [[ -n "$6" && "$6" != "-" ]]; then
   interval=$6
fi

start_uxtime=-1
if [[ -n "$7" && "$7" != "-" ]]; then
   start_uxtime=$7
fi

n_iter=1
if [[ -n "$8" && "$8" != "-" ]]; then
   n_iter=$8
fi

sleep_time=1800
if [[ -n "$9" && "$9" != "-" ]]; then
   sleep_time=$9
fi

pointing_interval=${interval}
if [[ -n "${10}" && "${10}" != "-" ]]; then
   pointing_interval=${10}
fi

repointing_resolution=30
if [[ -n "${11}" && "${11}" != "-" ]]; then
   repointing_resolution=${11}
fi

station=eda2
interface=enp216s0f0
if [[ -s /opt/aavs/config/station.yml ]]; then
   station=`awk -v station_section=0 '{if(index($1,":")>0 && NF==1){if(index($1,"station")>0 ){station_section=1;}else{station_section=0;}}if(station_section>0){if($1=="name:"){station_name=$2;gsub("\"","",station_name);gsub("\r","",station_name);print tolower(station_name);}}}' /opt/aavs/config/station.yml`
   echo "Station config file (or symbolik link) exists -> getting station_name = $station"
else
   echo "ERROR : /opt/aavs/config/station.yml file or symbolic link does not exist will use default station_name = $station or value passed in parameter -s"   
   exit;
fi
if [[ -n "${12}" && "${12}" != "-" ]]; then
   station=${12}
fi

ip=10.0.10.190
if [[ ${station} == "aavs2" ]]; then
   ip=10.0.10.210
fi
if [[ $station == "aavs3" ]]; then
   interface=enp65s0f0np0
   ip=10.137.0.103
fi



do_init_station=2
if [[ -n "${13}" && "${13}" != "-" ]]; then
   do_init_station=${13}
fi

full_time_resolution=1
if [[ -n "${14}" && "${14}" != "-" ]]; then
   full_time_resolution=${14}
fi

calibration_options=""
if [[ -n "${15}" && "${15}" != "-" ]]; then
   calibration_options=${15}
fi

point_station=1
if [[ -n "${16}" && "${16}" != "-" ]]; then
   point_station=${16}
fi

calibrate_station=1
if [[ -n "${17}" && "${17}" != "-" ]]; then
   calibrate_station=${17}
fi

daq_options=""
n_channels=40
if [[ -n "${18}" && "${18}" != "-" ]]; then
   n_channels=${18}
fi
end_channel=-1
if [[ $n_channels -gt 0 && $full_time_resolution -gt 0 ]]; then
   end_channel=$(($ch+$n_channels))
   daq_options="--start_channel 0 --nof_channels ${n_channels}"

   echo "INFO : adding extra DAQ options for acquire_station_beam program : $daq_options"
else
   echo "WARNING : no need for extra DAQ options (1 channel or not full time resolution ) for acquire_station_beam program : $daq_options"
fi

if [[ -n "${19}" && "${19}" != "-" ]]; then
   daq_options=${19}
fi

use_config_per_freq=0
if [[ -n "${20}" && "${20}" != "-" ]]; then
   use_config_per_freq=${20}
fi

config_file=${data_dir}/eda2_40channels_ch256.yml
if [[ ! -s $config_file ]]; then
   config_file=/data/triggering/eda2_40channels_ch256.yml
fi
if [[ -n "${21}" && "${21}" != "-" ]]; then
   config_file=${21}
fi

pointing_options="-"
if [[ -n "${22}" && "${22}" != "-" ]]; then
   pointing_options=${22}
fi

start_daq=1
if [[ -n "${23}" && "${23}" != "-" ]]; then
   start_daq=${23}   
fi

start_recording_ux=-1
if [[ -n "${24}" && "${24}" != "-" ]]; then
   start_recording_ux=${24}
fi

antfile=~/aavs-calibration/config/${station}/antenna_locations.txt
if [[ -n "${25}" && "${25}" != "-" ]]; then
   antfile="${25}"
fi


# RW/Chris Lee : I'm 99% sure the offset is 3 channels. (not 4). And that confirms the 3-channel offset also.
channel_from_start=4
ux=`date +%s`
start_uxtime_int=`echo $start_uxtime | awk '{printf("%d",$1);}'`
start_repointing=$(($start_uxtime_int+180))


echo "###################################################"
echo "PARAMETERS:"
echo "###################################################"
echo "station               = $station ( ip = $ip , interface = $interface )"
echo "interface            = $interface"
echo "freq_channel          = $freq_channel"
echo "data_dir              = $data_dir"
echo "interval              = $interval"
echo "pointing_interval     = $pointing_interval"
echo "start_uxtime          = $start_uxtime (start_repointing = $start_repointing)"
echo "n_iter                = $n_iter"
echo "sleep_time            = $sleep_time"
echo "repointing_resolution = $repointing_resolution"
echo "do_init_station       = $do_init_station"
echo "channel_from_start    = $channel_from_start"
echo "full_time_resolution  = $full_time_resolution"
echo "calibration_options   = $calibration_options"
echo "point_station         = $point_station"
echo "calibrate_station     = $calibrate_station"
echo "daq_options           = $daq_options"
echo "N channels = $n_channels -> end_channel = $end_channel"
echo "use_config_per_freq   = $use_config_per_freq"
echo "configuration file    = $config_file"
echo "ant positions file    = $antfile"
echo "current ux            = $ux"
echo "pointing_options      = $pointing_options"
echo "start_daq             = $start_daq"
echo "start_recording_ux    = $start_recording_ux (wait till this particular UNIX_TIME to start recording)" 
echo "ACQUIRE_STATION_BEAM_PATH = $ACQUIRE_STATION_BEAM_PATH"
echo "###################################################"

if [[ -s ${config_file} ]]; then
   echo "INFO : station configuration file $config_file OK"
else
   echo "ERROR : station configuration file $config_file does not exist -> cannot continue!"
   exit -1
fi

if [[ $start_uxtime_int -gt $ux ]]; then
   echo "Start unix time = $start_uxtime ($start_uxtime_int) -> waiting ..."
   which wait_for_unixtime.sh
   echo "wait_for_unixtime.sh $start_uxtime_int"
   wait_for_unixtime.sh $start_uxtime_int
fi


mkdir -p ${data_dir}
cd ${data_dir}

if [[ $do_init_station -gt 0 ]]; then
   echo "Initialising the station"

   # do_init_station=2 for the first time to accomdate for the bug
   while [[ $do_init_station -gt 0 ]];
   do
      # do initialisation :
      echo "python /opt/aavs/bin/station.py --config=$config_file -IPB"
      python /opt/aavs/bin/station.py --config=$config_file -IPB
      
      do_init_station=$(($do_init_station-1))
   done
   
   # TO BE FIXED !!!
   # TWICE DUE TO BUG !!!
   # echo "python /opt/aavs/bin/station.py --config=$config_file -IPB"
   # python /opt/aavs/bin/station.py --config=$config_file -IPB
else
  echo "WARNING : station initialisation is not required"
fi   


if [[ $calibrate_station -gt 0 ]]; then
  echo "~/aavs-calibration/station/calibrate_station.sh ${freq_channel} ${station} ${config_file} \"${calibration_options}\" $n_channels"
  ~/aavs-calibration/station/calibrate_station.sh ${freq_channel} ${station} ${config_file} "${calibration_options}" $n_channels
else
  echo "WARNING : station calibration is not required"
fi   

exit;

