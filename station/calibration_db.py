from __future__ import division
from future import standard_library
standard_library.install_aliases()
# from builtins import range
# from past.utils import old_div
# from pyaavs import station
import numpy 
import psycopg2
import logging
import time


def get_latest_delays( station_id, nof_antennas=256, debug=True, fittime=None, db_antennas_list=None ):
    """ Read latest coefficients from database """

    # Create connection to the calibration database.
    conn = psycopg2.connect(database='aavs')
    cur = conn.cursor()
    
    if db_antennas_list is None :
       db_antennas_list = list(range(0,nof_antennas))
    else :
       if nof_antennas != len(db_antennas_list) :
          print("ERROR in code : nof_antennas = %d, vs. list db_antennas_list has %d elements" % (nof_antennas,len(db_antennas_list)))
          return (None,None)

#    szSQL = "SELECT MAX(calibration_fit.fit_time) FROM calibration_fit,calibration_solution WHERE calibration_fit.fit_time=calibration_solution.fit_time and x_delay IS NOT NULL"
    if fittime is None :
       print("DEBUG : unspecified fittime -> calculating the latest calsol fittime")
       szSQL = "SELECT MAX(fit_time) FROM calibration_solution WHERE x_delay IS NOT NULL AND station_id={}".format(station_id)
       if debug :
          print("DEBUG : %s" % (szSQL))
       cur.execute( szSQL )
       fittime = cur.fetchone()[0]
       print("Latest calibration was at %s" % (fittime))
    else :
       print("DEBUG : calsolutuons fit time specified : %s" % (fittime))
       

    # Compute the required delays for the station beam channels

    # Grab antenna coefficients one by one (X pol)
    x_delays = numpy.zeros((nof_antennas, 2), dtype=numpy.float32)    
    ant_added_idx=0
    for ant_id in db_antennas_list:
        szSQL = "SELECT fit_time, x_delay, x_phase0 from calibration_solution WHERE x_delay IS NOT NULL AND station_id={} AND ant_id={} AND fit_time='''{}'''".format(station_id,ant_id,fittime)
        if debug :
           print("%d (%d) : %s" % (ant_id,ant_added_idx,szSQL))
           
        cur.execute( szSQL )
        x_delays[ant_added_idx, :] = cur.fetchone()[1:] # ant_added_idx may be different than ant_id for 15 or less TPMs 
        # print("Got fittime %s" % ())
        ant_added_idx += 1

    # Grab antenna coefficients one by one (Y pol)
    y_delays = numpy.zeros((nof_antennas, 2), dtype=numpy.float32)
    ant_added_idx=0
    for ant_id in db_antennas_list :
        szSQL = "SELECT fit_time, y_delay, y_phase0 from calibration_solution WHERE x_delay IS NOT NULL AND station_id={} AND ant_id={} AND fit_time='''{}'''".format( station_id,ant_id,fittime)
        if debug :
           print("%d (%d) : %s" % (ant_id,ant_added_idx,szSQL))
           
        cur.execute( szSQL )
        y_delays[ant_added_idx, :] = cur.fetchone()[1:] # ant_added_idx may be different than ant_id for 15 or less TPMs
        ant_added_idx += 1
        

    # Ready from database
    conn.close()

    # Return values
    return (x_delays,y_delays)

def get_latest_amps( station_id, freq_channel, nof_antennas=256, debug=True, max_amplitude=10.00, fittime=None, db_field_postfix="_amp", db_antennas_list=None ):
    """ Read latest coefficients from database """

    # Create connection to the calibration database.
    conn = psycopg2.connect(database='aavs')
    cur = conn.cursor()
    
    if db_antennas_list is None :
       db_antennas_list = list(range(0,nof_antennas))
    else :
       if nof_antennas != len(db_antennas_list) :
          print("ERROR in code : nof_antennas = %d, vs. list db_antennas_list has %d elements" % (nof_antennas,len(db_antennas_list)))
          return (None,None)

    if fittime is None :
       print("DEBUG : unspecified fittime -> calculating the latest calsol fittime")
#    szSQL = "SELECT MAX(calibration_fit.fit_time) FROM calibration_fit,calibration_solution WHERE calibration_fit.fit_time=calibration_solution.fit_time and x_delay IS NOT NULL"
       szSQL = "SELECT MAX(fit_time) FROM calibration_solution WHERE x_delay IS NOT NULL AND station_id={}".format(station_id)
       if debug :
          print("DEBUG : %s" % (szSQL))
       cur.execute( szSQL )
       fittime = cur.fetchone()[0]
       print("Latest calibration was at %s" % (fittime))
       
       out_f=open("fittime.txt","w")
       line = ("%s\n" % (fittime))
       out_f.write(line)
       out_f.close()
    else :
       print("DEBUG : calsolutuons fit time specified : %s" % (fittime))

    # Compute the required delays for the station beam channels

    # Grab antenna coefficients one by one (X pol)
    x_amp = numpy.zeros((nof_antennas), dtype=numpy.float32)
    ant_added_idx=0
    for ant_id in db_antennas_list:
        szSQL = "SELECT x{} from calibration_solution WHERE station_id={} AND ant_id={} AND fit_time='''{}'''".format(db_field_postfix,station_id,ant_id,fittime)
        if debug :
           print("%d (%d) : %s" % (ant_id,ant_added_idx,szSQL))
           
        cur.execute( szSQL )
        all_amps = cur.fetchone()[0]
        x_amp[ant_added_idx] = all_amps[freq_channel]
        if x_amp[ant_added_idx] > max_amplitude :
           print("WARNING : antenna = %d (%d) has amplitude %.4f in X pol. > threshold = %.4f -> setting to 0" % (ant_id,ant_added_idx,x_amp[ant_added_idx],max_amplitude))
           x_amp[ant_added_idx] = 0.00 # ant_added_idx may be different than ant_id for 15 or less TPMs
           
        ant_added_idx += 1   
        # print("Got fittime %s" % ())

    # Grab antenna coefficients one by one (Y pol)
    y_amp = numpy.zeros((nof_antennas), dtype=numpy.float32)
    ant_added_idx=0
    for ant_id in db_antennas_list:
        szSQL = "SELECT y{} from calibration_solution WHERE station_id={} AND ant_id={} AND fit_time='''{}'''".format(db_field_postfix,station_id,ant_id,fittime)
        if debug :
           print("%d (%d) : %s" % (ant_id,ant_added_idx,szSQL))
           
        cur.execute( szSQL )
        all_amps = cur.fetchone()[0]
        y_amp[ant_added_idx] = all_amps[freq_channel]
        if y_amp[ant_added_idx] > max_amplitude :
           print("WARNING : antenna = %d (%d) has amplitude %.4f in Y pol. > threshold = %.4f -> setting to 0" % (ant_id,ant_added_idx,y_amp[ant_added_idx],max_amplitude))
           y_amp[ant_added_idx] = 0.00
        
        
        ant_added_idx += 1

    # Ready from database
    conn.close()
    
    if debug :
       print("DEBUG : calibration amplitudes for channel = %d:" % (freq_channel))
       print(" ANT    |    AMP_X    |    AMP_Y    |   ANT_DB_INDEX ")
       ant_added_idx=0
       for ant_id in db_antennas_list:
          print(" %d         %.3f         %.3f        %d" % (ant_added_idx,x_amp[ant_added_idx],y_amp[ant_added_idx],ant_id))
          ant_added_idx += 1

    # Return values
    return (x_amp,y_amp)

