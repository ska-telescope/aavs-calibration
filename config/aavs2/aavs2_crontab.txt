# Edit this file to introduce tasks to be run by cron.
# 
# Each task to run has to be defined through a single line
# indicating with different fields when the task will be run
# and what command to run for the task
# 
# To define the time you can provide concrete values for
# minute (m), hour (h), day of month (dom), month (mon),
# and day of week (dow) or use '*' in these fields (for 'any').# 
# Notice that tasks will be started based on the cron's system
# daemon's notion of time and timezones.
# 
# Output of the crontab jobs (including errors) is sent through
# email to the user the crontab file belongs to (unless redirected).
# 
# For example, you can run a backup of all your user accounts
# at 5 a.m every week with:
# 0 5 * * 1 tar -zcf /var/backups/home.tgz /home/
# 
# For more information see the manual pages of crontab(5) and cron(8)
# 
# m h  dom mon dow   command
# 0 15 * * * rsync -avP /data/2021_06_2* nimbus5:/data_archive/ > /tmp/rsync_nimbus5_copy1.log 2>&1 &
# 0 21 * * * rsync -avP /data/2021_08*puls* nimbus5:/data_archive/ > /tmp/rsync_nimbus5_copy2.log 2>&1 &
# 0 9 * * * rsync -avP /data/chris_lee/2021011[5,6,7,8,9] nimbus5:/data/chris_lee/ > /tmp/rsync_nimbus5.log 2>&1 &
12 7 * * * rsync -avP /data/2023_10_??_pulsars* nimbus5:/data/ >> /data/log/rsync_nimbus_202309.log 2>&1 &
# 15 14 * * * rsync -avP /data/2023_0[8,9]_3?_pulsars nimbus5:/data_archive/ >> /data/log/rsync_nimbus_afternoon.log 2>&1 &
# 35 9 * * * rsync -avP /data/2023_01_??_pulsars_crab nimbus5:/data_archive/ >> /data/log/rsync_nimbus_crab.log 2>&1 &
# 0 21 29 11 * rsync -avP /data/2022_11_29_repointing_test_5s_30s_60s nimbus5:/data_archive/ >> /data/log/rsync_nimbus_20221130.log 2>&1 &
#
# Copy antenna health monitoring from aavs1-server :
0 6 * * * mkdir -p /data/monitoring/;rsync -avP aavs1-server:/storage/monitoring/antenna_health /data/monitoring/ > /tmp/rsync_aavs1server.log 2>&1 &
# 0 4 14 6 * rsync --exclude '*.hdf5' -avP /data/2023_06_13_ch415_corr msok@mwa-process02.cira.curtin.edu.au:/transdata/msok/aavs2/ > /tmp/rsync_mwa02_20230613.log 2>&1 &
#
# daily calibration 12:10 
# 10 12 * * * bash /home/aavs/aavs-calibration/start_calibration_aavs2.sh > /data/log/start_calibration_aavs2.log 2>&1 &

