import numpy
import copy
import sys

start_ant=0
ants_per_tpm=16

tpms={}

tpm=0
while tpm < 16 :
   ant_start=tpm*ants_per_tpm
   ant_end=ant_start+ants_per_tpm-1
   print("TPM%d : %d - %d" % ((tpm+1),ant_start,ant_end))
   
   tpms[tpm] = numpy.arange(ant_start,ant_end+1)
   
   tpm += 1

print("\n\n")
print("Verification:")
for tpm in range(0,16):
   print("TPM%d : %d - %d" % (tpm,tpms[tpm][0],tpms[tpm][15]))   
   
print("\n\n")

# ~/aavs-calibration/config/eda2$ awk '{if(substr($1,1,1)!="#" && $5=="1"){print $2;}}' instr_config.txt | sort -n -u | awk '{printf("%s,",$1);}'
original_flags=[15,20,22,28,32,33,34,35,36,37,38,39,44,48,49,50,51,52,53,54,55,66,69,74,79,86,87,90,96,97,98,99,100,101,102,103,127,136,141,142,143,146,155,159,178,181,182,190,202,207,223,244]   

# 0 based 
excluded_tpms=[3-1,7-1,13-1] # to flag 1-based TPMs 3, 7 and 13 

all_ants=numpy.arange(0,256)

# set -1 for excluded TPMs:
for tpm in excluded_tpms :
   for tpm_ant in tpms[tpm] :
      all_ants[tpm_ant] = -1
      
# get the flagged antenna indexes , not counting excluded TPMs
print("Flagged antenna indexes after exluding broken TPMs :")
new_idx=0
for ant_idx in range(0,len(all_ants)) :
   if all_ants[ant_idx] >= 0 :
      if all_ants[ant_idx] in original_flags :
         sys.stdout.write("%d," % (new_idx))
  
      # new index is only incremented for antennas not belonging to excluded TPMs :
      new_idx += 1

# flagged_ants=copy.copy(original_flags)
#for tpm in excluded_tpms :
#
#   new_flagged_ants=[]
#   for flagged_ant in flagged_ants :
#      if flagged_ant < tpms[tpm][0] or flagged_ant > tpms[tpm][15] :
#         if flagged_ant < tpms[tpm][0] :
#            new_ant_index = flagged_ant
#         elif flagged_ant > tpms[tpm][15] :
#            new_ant_index = flagged_ant - 16 
#
#         new_flagged_ants.append( new_ant_index )            
#      else :
#         print("TPM%d , antenna %d is flagged -> skipped" % ((tpm+1),flagged_ant))                

   # flagged_ants=copy.copy(new_flagged_ants)

# print("Flagged antennas:")
# print("" % end="")
# for ant in flagged_ants :
  # print("%d," % ant)
#  sys.stdout.write("%d," % ant)
  